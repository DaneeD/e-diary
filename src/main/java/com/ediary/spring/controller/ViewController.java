package com.ediary.spring.controller;

import java.time.LocalDate;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ediary.spring.entity.Grade;
import com.ediary.spring.entity.Role;
import com.ediary.spring.entity.User;
import com.ediary.spring.service.EdiaryService;

@Controller
public class ViewController {
	
	@Autowired
	private EdiaryService service;
	
	@GetMapping("/admin")
	public String getAdmin(Model model) {
		model.addAttribute("teachers", service.listUsersByRoleType(Role.TEACHER.name()));
		model.addAttribute("students", service.listUsersByRoleType(Role.STUDENT.name()));
		model.addAttribute("parents", service.listUsersByRoleType(Role.PARENT.name()));
		model.addAttribute("roles", Arrays.asList(Role.TEACHER.name(), Role.STUDENT.name(), Role.PARENT.name()));
		return "admin";
	}

	@GetMapping("/admin/register-user/{roles[?]}")
	public String addUser(Model model, @PathVariable("roles[?]") String roles) {
		model.addAttribute("user", new User());
		model.addAttribute("actualRole", roles);
		model.addAttribute("childName");
		return "register-user";
	}

	@PostMapping("/admin/register-user/{actualRole}")
	public String doAddUser(@RequestParam(name = "username") String username,
			@RequestParam(name = "userPassword") String userPassword, @RequestParam(name = "fullname") String fullname,
			@RequestParam(name = "email") String email, @PathVariable String actualRole, Model model,
			@ModelAttribute("childName") String childName) {

		try {
			if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(userPassword)
					&& StringUtils.isNotBlank(fullname) && StringUtils.isNotBlank(email)) {
				User newUserToDb = new User(username, userPassword, fullname, email, true, false);
				switch (actualRole) {
				case "TEACHER":
					if(!service.checkIfUserExists(username)) {
						setUserData(newUserToDb, Role.TEACHER);
						return "redirect:/admin";
					}
					break;
				case "STUDENT":
					if(!service.checkIfUserExists(username)) {
						setUserData(newUserToDb, Role.STUDENT);
						return "redirect:/admin";
					}
					break;
				case "PARENT":
					if (StringUtils.isNotBlank(childName) && service.validateChild(childName) && !service.checkIfUserExists(username)) {
						setUserData(newUserToDb, Role.PARENT);
						service.addChildToUser(username, childName);
						return "redirect:/admin";
					}
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("Error happend during saving new User to database:\n" + e.getMessage());
		}
		return "redirect:/admin/register-user/{actualRole}";
	}

	@GetMapping("/admin/activation-switch/{activated}/{userId}")
	public String activationSwitch(@PathVariable boolean activated, @PathVariable Integer userId) {
		service.switchActivation(activated, userId);
		return "redirect:/admin";
	}

	@GetMapping("/admin/block-switch/{blocked}/{userId}")
	public String blockSwitch(@PathVariable boolean blocked, @PathVariable Integer userId) {
		service.switchBlock(blocked, userId);
		return "redirect:/admin";
	}

	@GetMapping("/basic")
	public String getBasic(Model model) {
		String username = getUsername();
		Integer userId = service.getUserIdByName(username);

		model.addAttribute("username", username);
		model.addAttribute("userId", userId);
		model.addAttribute("welcomeMsg", "Welcome " + username);
		model.addAttribute("subjects", service.listUserSubjects(userId));
		return "basic";
	}

	@GetMapping("/basic/subject/{subjectName}")
	public String showSubject(@PathVariable String subjectName, Model model) {
		String username = getUsername();
		Integer userId = service.getUserIdByName(username);

		model.addAttribute("subjectName", subjectName);
		model.addAttribute("grades", service.listUserGrades(subjectName, userId));
		return "basic-subject";
	}

	@GetMapping("/teacher")
	public String getTeacher(Model model) {
		model.addAttribute("students", service.listUsersByRole(2));
		return "teacher";
	}

	@GetMapping("/teacher/student/{studentId}")
	public String getTeachersStudent(@PathVariable Integer studentId, Model model) {
		model.addAttribute("studentId", studentId);
		model.addAttribute("subjects", service.listUserSubjects(studentId));
		return "basic";
	}

	@GetMapping("/teacher/student/{studentId}/{subjectName}")
	public String getTeachersStudentsGrades(@PathVariable String subjectName, @PathVariable Integer studentId,
			Model model) {
		model.addAttribute("grades", service.listUserGrades(subjectName, studentId));
		model.addAttribute("subjectName", subjectName);
		return "basic-subject";
	}

	@GetMapping("/teacher/student/teacher-addgrade/{studentId}/{subjectName}")
	public String addGrade(@PathVariable String subjectName, @PathVariable Integer studentId, Model model) {
		model.addAttribute("grade", new Grade());
		return "teacher-addgrade";
	}

	@PostMapping("/teacher/student/teacher-addgrade/{studentId}/{subjectName}")
	public String doAddGrade(@PathVariable String subjectName, @PathVariable Integer studentId,
			@RequestParam(name = "date") String date, @RequestParam(name = "description") String desc,
			@RequestParam(name = "value") String value, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (StringUtils.isNotBlank(date) && StringUtils.isNotBlank(desc) && StringUtils.isNotBlank(value)) {
			try {
				service.addGrade(new Grade(LocalDate.parse(date), desc, Integer.parseInt(value), auth.getName()),
						subjectName, studentId);
				return "redirect:/teacher/student/{studentId}/{subjectName}";
			} catch (Exception e) {
				System.out.println("Parsing exception happened: \n" + e.getMessage());
			}
		}
		return "redirect:/teacher/student/teacher-addgrade/{studentId}/{subjectName}";
	}

	@GetMapping("/teacher/student/teacher-addsubject/{studentId}")
	public String addSubject(@PathVariable Integer studentId, Model model) {
		model.addAttribute("grade", new Grade());
		model.addAttribute("subjectName", "");
		model.addAttribute("studentId", studentId);
		return "teacher-addsubject";
	}

	@PostMapping("/teacher/student/teacher-addsubject/{studentId}")
	public String doAddSubject(@PathVariable Integer studentId, @ModelAttribute(name = "subjectName") String subjectName,
			@RequestParam(name = "date") String date, @RequestParam(name = "description") String desc,
			@RequestParam(name = "value") String value, Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (service.validateSubjectName(subjectName) && StringUtils.isNotBlank(date) && StringUtils.isNotBlank(desc) && StringUtils.isNotBlank(value)) {
			try {
				service.addGrade(new Grade(LocalDate.parse(date), desc, Integer.parseInt(value), auth.getName()),
						subjectName, studentId);
				return "redirect:/teacher/student/{studentId}";
			} catch (Exception e) {
				System.out.println("Parsing exception happened: \n" + e.getMessage());
			}
		}
		return "redirect:/teacher/student/teacher-addsubject/{studentId}";
	}

	private String getUsername() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		try {
			if (!(auth instanceof AnonymousAuthenticationToken)) {
				String currentUsername = auth.getName();
				if (service.getUserRoleId(currentUsername) == Role.PARENT.getRoleId()) {
					return service.getChildNameByParentName(currentUsername);
				}
				return currentUsername;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return "";
	}
	
	private void setUserData(User user, Role role) {
		user.setRole(role);
		service.addUser(user);
		service.addUserRole(user);
	}
}
