package com.ediary.spring.entity;

public enum Role {
	ADMIN(1),STUDENT(2),PARENT(3),TEACHER(4);
	
	public final Integer roleId;
	
	private Role(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getRoleId() {
		return roleId;
	}
}
