package com.ediary.spring.entity;

public class Subject {
	private Integer subjectId;
	private String subjectName;
	private Double gradeAvg;
	private Integer finalGrade;
	
	public Subject() {
		
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Double getGradeAvg() {
		return gradeAvg;
	}

	public void setGradeAvg(Double gradeAvg) {
		this.gradeAvg = gradeAvg;
	}

	public Integer getFinalGrade() {
		return finalGrade;
	}

	public void setFinalGrade(Integer finalGrade) {
		this.finalGrade = finalGrade;
	}

	@Override
	public String toString() {
		return "Subject [subjectId=" + subjectId + ", subjectName=" + subjectName + ", gradeAvg=" + gradeAvg
				+ ", finalGrade=" + finalGrade + "]";
	}
	
	
	
}
