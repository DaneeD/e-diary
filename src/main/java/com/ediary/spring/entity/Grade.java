package com.ediary.spring.entity;

import java.time.LocalDate;

public class Grade {
	private LocalDate date;
	private String description;
	private Integer value;
	private String teacherName;
	
	public Grade() {

	}

	public Grade(LocalDate date, String description, Integer value, String teacherName) {
		this.date = date;
		this.description = description;
		this.value = value;
		this.teacherName = teacherName;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	
	@Override
	public String toString() {
		return "Grade [date=" + date + ", description=" + description + ", value=" + value + ", teacherName="
				+ teacherName + "]";
	}

}
