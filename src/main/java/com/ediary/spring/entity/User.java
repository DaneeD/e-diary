package com.ediary.spring.entity;

public class User {
	private int userId;
	private String username;
	private String userPassword;
	private String fullname;
	private String email;
	private Role role;
	private boolean activated;
	private boolean blocked;

	public User() {

	}

	public User(String username, String userPassword, String fullname, String email, boolean activated, boolean blocked) {
		this.username = username;
		this.userPassword = userPassword;
		this.fullname = fullname;
		this.email = email;
		this.activated = activated;
		this.blocked = blocked;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", userPassword=" + userPassword + ", fullname="
				+ fullname + ", email=" + email + ", role=" + role + ", activated=" + activated + ", blocked=" + blocked
				+ "]";
	}

}
