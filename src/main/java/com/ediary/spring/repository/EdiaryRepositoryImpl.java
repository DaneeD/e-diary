package com.ediary.spring.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import com.ediary.spring.entity.Grade;
import com.ediary.spring.entity.Subject;
import com.ediary.spring.entity.User;

@Repository
public class EdiaryRepositoryImpl implements EdiaryRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	public void addUser(User user) {
		String query = "INSERT INTO users (username, user_password, fullname, email, activated, blocked) VALUES (?,?,?,?,?,?)";
		passwordEncoder.encode(user.getUserPassword());
		try {
			jdbcTemplate.update(query, ps -> {
				ps.setString(1, user.getUsername());
				ps.setString(2, passwordEncoder.encode(user.getUserPassword()));
				ps.setString(3, user.getFullname());
				ps.setString(4, user.getEmail());
				ps.setBoolean(5, user.isActivated());
				ps.setBoolean(6, user.isBlocked());
			});
		} catch (DataAccessException e) {
			System.out.println("Error happened during inserting new User into Database:\n" + e.getMessage());
		}
	}

	public boolean checkIfUserExists(String username) {
		String query = "SELECT user_id FROM users WHERE username = ?";
		Integer count = 0;
		try {
			count = jdbcTemplate.queryForObject(query, SingleColumnRowMapper.newInstance(Integer.class), username);
			if(count > 0) {
				return true;
			}
		} catch (DataAccessException e) {
			System.out.println("No username matches found in database:\n" + e.getMessage());
		}
		return false;
	}
	
	public void addUserRole(User user) {
		String query = "INSERT INTO user_roles (user_id, role_id) VALUES ((SELECT user_id FROM users WHERE username = ?),?)";
		try {
			jdbcTemplate.update(query, ps -> {
				ps.setString(1, user.getUsername());
				ps.setInt(2, user.getRole().getRoleId());
			});
		} catch (DataAccessException e) {
			System.out.println("Error happened during inserting new value into user_roles table:\n" + e.getMessage());
		}
	}

	public String getChildNameByParentName(String parentName) {
		String query = "SELECT username FROM users WHERE user_id = (SELECT child_id FROM users WHERE username = ?)";
		return jdbcTemplate.queryForObject(query, SingleColumnRowMapper.newInstance(String.class), parentName);
	}

	public Integer getUserIdByName(String username) {
		String query = "SELECT user_id FROM users WHERE username = ?";
		return jdbcTemplate.queryForObject(query, SingleColumnRowMapper.newInstance(Integer.class), username);
	}

	public List<Subject> listUserSubjects(Integer studentId) {
		String query = "SELECT subject_name, AVG(grade) AS gradeAvg, ROUND(AVG(grade), 0) AS finalGrade FROM subject_grade sg "
				+ "JOIN subject s ON sg.subject_id = s.subject_id WHERE sg.user_id = ? GROUP BY s.subject_id";
		return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Subject.class), studentId);
	}

	public List<Grade> listUserGrades(String subjectName, Integer studentId) {
		String query = "SELECT grade_date AS date, description, grade AS value, u.fullname AS teacherName FROM subject_grade sg "
				+ "JOIN subject s ON sg.subject_id = s.subject_id " + "JOIN users u ON sg.teacher_id = u.user_id "
				+ "WHERE subject_name = ? AND sg.user_id = ?";
		return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Grade.class), subjectName, studentId);
	}

	public Integer getUserRoleId(String username) {
		String query = "SELECT role_id FROM user_roles ur JOIN users u ON ur.user_id = u.user_id WHERE u.username = ?";
		return jdbcTemplate.queryForObject(query, SingleColumnRowMapper.newInstance(Integer.class), username);
	}

	public List<User> listUsersByRole(Integer roleId) {
		String query = "SELECT u.user_id, u.username, u.fullname FROM users u JOIN user_roles ur ON u.user_id = ur.user_id WHERE role_id = ?";
		return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(User.class), roleId);
	}

	public void addGrade(Grade grade, String subjectName, Integer studentId) {
		String subjectIdQuery = "SELECT subject_id FROM subject WHERE subject_name = ?";
		String insertQuery = "INSERT INTO subject_grade (subject_id, user_id, grade_date, description, teacher_id, grade) VALUES (?,?,?,?,?,?)";
		Integer subjectId = jdbcTemplate.queryForObject(subjectIdQuery,
				SingleColumnRowMapper.newInstance(Integer.class), subjectName);

		jdbcTemplate.update(insertQuery, pss -> {
			pss.setInt(1, subjectId);
			pss.setInt(2, studentId);
			pss.setDate(3, Date.valueOf(grade.getDate()));
			pss.setString(4, grade.getDescription());
			pss.setInt(5, getUserIdByName(grade.getTeacherName()));
			pss.setInt(6, grade.getValue());
		});
	}

	public List<User> listUsersByRoleType(String roleType) {
		String query = "SELECT u.user_id, fullname, activated, blocked FROM users u JOIN user_roles ur ON "
				+ "u.user_id = ur.user_id JOIN role r ON ur.role_id = r.role_id WHERE role_type = ?";
		return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(User.class), roleType);
	}

	public boolean validateSubjectName(String subjectName) {
		String query = "SELECT subject_id FROM subject WHERE subject_name = ?";
		Integer count = 0;
		try {
			count = jdbcTemplate.queryForObject(query, SingleColumnRowMapper.newInstance(Integer.class), subjectName);
			if(count > 0) {
				return true;
			}
		} catch (DataAccessException e) {
			System.out.println("No subject name matches found in database:\n" + e.getMessage());
		}
		return false;
	}
	
	public boolean validateChild(String childName) {
		String query = "SELECT activated, blocked, role_type AS role FROM users u JOIN user_roles ur ON u.user_id = ur.user_id JOIN role r ON ur.role_id = r.role_id WHERE username = ?";
		User user = jdbcTemplate.queryForObject(query, BeanPropertyRowMapper.newInstance(User.class), childName);

		if (user != null && user.isActivated() && !user.isBlocked() && user.getRole().name().equals("STUDENT")) {
			return true;
		}
		return false;
	}

	public void addChildToUser(String username, String childName) {
		String query = "UPDATE users SET child_id = (SELECT user_id FROM users WHERE username = ?) WHERE username = ?";
		jdbcTemplate.update(query, childName, username);
	}

	public void switchActivation(boolean activated, Integer userId) {
		String query = "UPDATE users SET activated = ? WHERE user_id = ?";
		jdbcTemplate.update(query, activated, userId);
	}

	public void switchBlock(boolean blocked, Integer userId) {
		String query = "UPDATE users SET blocked = ? WHERE user_id = ?";
		jdbcTemplate.update(query, blocked, userId);
	}

}
