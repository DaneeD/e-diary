package com.ediary.spring.repository;

import java.util.List;

import com.ediary.spring.entity.Grade;
import com.ediary.spring.entity.Subject;
import com.ediary.spring.entity.User;

public interface EdiaryRepository {
	
	public void addUser(User user);
	
	public boolean checkIfUserExists(String username);
	
	public void addUserRole(User user);
	
	public String getChildNameByParentName(String parentName);
	
	public Integer getUserIdByName(String username);
	
	public List<Subject> listUserSubjects(Integer studentId);
	
	public List<Grade> listUserGrades(String subjectName, Integer studentId);
	
	public Integer getUserRoleId(String username);
	
	public List<User> listUsersByRole(Integer roleId);
	
	public void addGrade(Grade grade, String subjectName, Integer studentId);
	
	public List<User> listUsersByRoleType(String roleType);
	
	public boolean validateSubjectName(String subjectName);
	
	public boolean validateChild(String childName);
	
	public void addChildToUser(String username, String childName);
	
	public void switchActivation(boolean activated, Integer userId);
	
	public void switchBlock(boolean blocked, Integer userId);

}
