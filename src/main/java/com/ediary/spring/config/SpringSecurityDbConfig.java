package com.ediary.spring.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.ediary.spring.entity.Role;

@Configuration
public class SpringSecurityDbConfig extends WebSecurityConfigurerAdapter {

	private final String adminRoleId = String.valueOf(Role.ADMIN.getRoleId());
	private final String studentRoleId = String.valueOf(Role.STUDENT.getRoleId());
	private final String parentRoleId = String.valueOf(Role.PARENT.getRoleId());
	private final String teacherRoleId = String.valueOf(Role.TEACHER.getRoleId());
	
	@Autowired
    private DataSource dataSource;
	
	@Override
 	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.authenticationProvider(authProvider())
		.authorizeRequests()
			.antMatchers("/").permitAll()
			.antMatchers("/login").permitAll()
			.antMatchers("/admin/**").hasAnyAuthority(adminRoleId)
			.antMatchers("/basic/**").hasAnyAuthority(studentRoleId, parentRoleId)
			.antMatchers("/teacher/**").hasAnyAuthority(teacherRoleId)
			.antMatchers("/index").fullyAuthenticated()
		.and()
		.formLogin()
			.loginPage("/login")
			.defaultSuccessUrl("/index", true)	
			.usernameParameter("username").passwordParameter("password")
			.permitAll()
		.and()
		.logout()
			.invalidateHttpSession(true)
			.clearAuthentication(true)
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.logoutSuccessUrl("/login?loggedout")
			.permitAll();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
		.usersByUsernameQuery("SELECT username, user_password, 1 AS ENABLED FROM users WHERE username = ?")
		.authoritiesByUsernameQuery("SELECT u.username, ur.role_id, u.activated FROM users u JOIN user_roles ur ON u.user_id = ur.user_id WHERE username = ? AND activated = 1");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}
	
}
