package com.ediary.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ediary.spring.entity.Grade;
import com.ediary.spring.entity.Subject;
import com.ediary.spring.entity.User;
import com.ediary.spring.repository.EdiaryRepository;


@Service
public class EdiaryService {

	@Autowired
	private EdiaryRepository repository;
	
	public void addUser(User user) {
		repository.addUser(user);
	}

	public boolean checkIfUserExists(String username) {
		return repository.checkIfUserExists(username);
	}
	
	public void addUserRole(User user) {
		repository.addUserRole(user);
	}

	public Integer getUserIdByName(String username) {
		return repository.getUserIdByName(username);
	}

	public List<Subject> listUserSubjects(Integer studentId) {
		return repository.listUserSubjects(studentId);
	}

	public List<Grade> listUserGrades(String subjectName, Integer userId) {
		return repository.listUserGrades(subjectName, userId);
	}

	public Integer getUserRoleId(String username) {
		return repository.getUserRoleId(username);
	}

	public String getChildNameByParentName(String parentName) {
		return repository.getChildNameByParentName(parentName);
	}

	public List<User> listUsersByRole(Integer roleId) {
		return repository.listUsersByRole(roleId);
	}

	public void addGrade(Grade grade, String subjectName, Integer studentId) {
		repository.addGrade(grade, subjectName, studentId);
	}

	public List<User> listUsersByRoleType(String roleType) {
		return repository.listUsersByRoleType(roleType);
	}

	public boolean validateSubjectName(String subjectName) {
		return repository.validateSubjectName(subjectName);
	}
	
	public boolean validateChild(String childName) {
		return repository.validateChild(childName);
	}

	public void addChildToUser(String username, String childName) {
		repository.addChildToUser(username, childName);
	}

	public void switchActivation(boolean activated, Integer userId) {
		activated = !activated;
		repository.switchActivation(activated, userId);
	}

	public void switchBlock(boolean blocked, Integer userId) {
		blocked = !blocked;
		repository.switchBlock(blocked, userId);
		repository.switchActivation(!blocked, userId);
	}

}
