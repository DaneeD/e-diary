# ***E-diary***

Basic Spring Boot web application created for handling and overseeing students's subjects and grades for multiple roles.

## Description
This application as the most standard usage gives access to students to their studying performance as they can check specific subject's grade average and also current status of their overall evaluation. Clicking on a subject's name one can list out the grades, date, topic description and related teacher's name. In addition student's parents can also be registered so they can reach their children's data.

Signing in as a teacher gives access to the list of students. Clicking on a student's name the teacher can add a new subject, or if it exists a new grade to a topic.

The administrator can see all the users listed by their roles. They can inactivate or block specific users.
At current development level only the administrator can add new users to the application.

The users with different roles can sign in through the login page handled by Spring Security connected to the MySQL database. Passwords are stored encoded in the database. After signing in only pages are belonging to the spicific role can be reached.

## Project details
***Java Spring Boot project***

### Dependencies

- Spring Boot starter 
    - web
    - tomcat
    - data jdbc
    - security
    - thymeleaf
- MySql connector
- Spring Security 5 - Thymeleaf extras
- Commons-lang3

### Database
Application data is stored in phpMyAdmin MySQL database in five tables:

- **role:** role types
- **users:** storing basic user data, including encoded passwords, student-parent connections
- **user roles:** connecting users with their roles
- **subject:** subject types
- **subject grade:** storing grades belonging to specific subjects and students

Database reached through application properties parameters.

### Usage - screenshots

Login page

![Login page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot01.jpg)


Administrator’s page – inactivate, block and add new user functions

![Admin page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot02a.jpg)

![Admin page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot03a.jpg)

Student’s and parent’s main page

![Student page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot04s.jpg)

Detailed subject and grade page for students

![Student page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot05s.jpg)

Teacher’s main page – list of students

![Teacher page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot06t.jpg)

Student’s subjects listed with the add new subject and grade function

![Teacher page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot07t.jpg)

![Teacher page](https://bitbucket.org/DaneeD/e-diary/raw/master/docs/images/screenshot08t.jpg)

## Version
Initial Release


## Author

**Daniel Dancs** , 6 June 2020